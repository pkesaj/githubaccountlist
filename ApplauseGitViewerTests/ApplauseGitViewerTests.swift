//
//  ApplauseGitViewerTests.swift
//  ApplauseGitViewerTests
//
//  Created by mac on 26/06/2019.
//  Copyright © 2019 mac. All rights reserved.
//

import XCTest
@testable import ApplauseGitViewer

class ApplauseGitViewerTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    private func checkAPIRequest(_ error: API.RequestErrors?) -> (result: Bool, reason: String?){
        if let error = error {
            switch error {
            case .decodingError(let decodingError):
                return (false, "Decoding error \(decodingError)")
            case .noData, .badURL(urlString: _), .someError:
                return (false, "Unexpected error")
            case .noInternetConnection:
                return (false, "No internet connection")
            default: break
            }
        } else {
            return (true, nil)
        }
        return (true, nil)
    }
    
    func testGithubRepoModelElement() {
        
        let exp = self.expectation(description: "Request")
        
        API.load(API.Router<[GithubRepoModelElement]>.getRepoList(accountName: Statics.gitHubAccountName)) { (responseData, error) in
            
            let errorCheckResult = self.checkAPIRequest(error)
            
            if !errorCheckResult.result {
                XCTAssert(errorCheckResult.result, errorCheckResult.reason ?? "")
            }
            
            XCTAssert(responseData != nil, "Response `GithubRepoModelElement` is nil")
            exp.fulfill()
        }
        
        waitForExpectations(timeout: 10, handler: nil)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
