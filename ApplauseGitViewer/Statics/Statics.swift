//
//  Statics.swift
//  ApplauseGitViewer
//
//  Created by mac on 26/06/2019.
//  Copyright © 2019 mac. All rights reserved.
//

import Foundation

struct Statics {
    
    static let reposLimit: Int = 10
    static let gitHubAccountName = "ApplauseOSS"
}
