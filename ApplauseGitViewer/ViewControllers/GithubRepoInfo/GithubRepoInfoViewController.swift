//
//  GithubRepoInfoViewController.swift
//  ApplauseGitViewer
//
//  Created by mac on 26/06/2019.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class GithubRepoInfoViewController: UIViewController {
    
    @IBOutlet weak var textView: UITextView! {
        didSet {
            textView.text = prepareGithubInfo()
            textView.isEditable = false
        }
    }
    
    var model: GithubRepoModelElement? {
        didSet {
            guard self.textView != nil else { return }
            DispatchQueue.main.async {
                self.textView.text = self.prepareGithubInfo()
            }
        }
    }
    
    private func prepareGithubInfo() -> String {
        guard let model = model else { return "Unexpected no info.\n" }
        
        var info = ""
        
        if let fullName = model.fullName {
            info += "Name \(fullName)\n"
        }
        if let defaultBranch = model.defaultBranch {
            info += "Default branch: \(defaultBranch)\n\n"
        }
        if let githubRepoModelDescription = model.githubRepoModelDescription {
            info += "Description: \(githubRepoModelDescription)\n"
        }
        
        return info
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
