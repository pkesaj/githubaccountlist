//
//  ViewController.swift
//  ApplauseGitViewer
//
//  Created by mac on 26/06/2019.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let accountName = Statics.gitHubAccountName
    
    private weak var collectionView: UICollectionView?
    
    var model: [GithubRepoModelElement]? {
        didSet {
            filteredModel = model ?? []
            DispatchQueue.main.async {
                self.collectionView?.reloadData()
            }
        }
    }
    
    var filteredModel: [GithubRepoModelElement] = []
    var filterValue = "" {
        didSet {
            updateElements()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        loadDataIntoVC()
    }
    
    private func setupCollectionView() {
        guard collectionView == nil else { return }
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 1
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.addSubview(collectionView)
        self.collectionView = collectionView
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        collectionView.backgroundColor = .white
        
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        collectionView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        registerCells()
    }
    
    private func updateElements() {
        
        if filterValue.count > 0 {
            filteredModel = model?.filter({
                return $0.fullName?.lowercased().contains(filterValue.lowercased()) ?? true
            }) ?? []
        } else {
            filteredModel = model ?? []
        }
        DispatchQueue.main.async {
            self.collectionView?.reloadSections(IndexSet(arrayLiteral: 1))
        }
    }
    
    private func loadDataIntoVC() {
        
        API.load(API.Router<[GithubRepoModelElement]>.getRepoList(accountName: accountName)) { [weak self] (responseData, error) in
            guard let self = self else {
                return // alert or sth
            }
            
            //error handling
            guard error == nil, let response = responseData else {
                //aleeeeerts or badges or info
                return
            }
            
            self.model = Array(response.prefix(Statics.reposLimit))
            self.filterValue = ""
        }
    }
    
    private func registerCells() {
        //R.swift library should be here.
        collectionView?.register(UINib(nibName: GithubRepoCell.identifier, bundle: nil), forCellWithReuseIdentifier: GithubRepoCell.identifier)
        collectionView?.register(SearchCell.self, forCellWithReuseIdentifier: SearchCell.identifier)
    }
}

extension ViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2 // 0 - search, 1 - githubCells
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard section > 0 else { return 1 }
        return filteredModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SearchCell.identifier, for: indexPath)
            (cell as? SearchCell)?.delegate = self
            return cell
        } else {
            //R.swift and we can force.
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GithubRepoCell.identifier, for: indexPath)
            
            if let wrappedCell = cell as? GithubRepoCell,
                self.filteredModel.count > indexPath.row {
                let cellModel = self.filteredModel[indexPath.row]
                wrappedCell.model = (cellModel.id, cellModel.fullName)
            }
            
            return cell
        }
    }
}

extension ViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard indexPath.section == 1, self.filteredModel.count > indexPath.row else { return }
        
        //R.swift
        guard let viewController = UIStoryboard(name: "GithubRepoInfoViewController", bundle: nil).instantiateInitialViewController() as? GithubRepoInfoViewController else { return }
        
        viewController.model = self.filteredModel[indexPath.row]
        navigationController?.pushViewController(viewController, animated: true)
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            return SearchCell.getCellSize(collectionView)
        } else {
            return GithubRepoCell.getCellSize(collectionView)
        }
    }
}

extension ViewController: SearchCellDelegate {
    
    func searchValueChanged(_ searchField: UISearchBar?) {
        filterValue = searchField?.text ?? ""
    }
}
