//
//  API.swift
//  ApplauseGitViewer
//
//  Created by mac on 26/06/2019.
//  Copyright © 2019 mac. All rights reserved.
//

import Foundation

struct API {
    
    enum RequestErrors {
        case noInternetConnection
        case badURL(urlString: String)
        case someError // dont have time for perfect error handling.
        case decodingError(Error)
        case noData // probably server is down.
        case successError(Error)
    }
    
    struct Environment {
        static let apiUrlProd = "https://api.github.com"
    }
    
    enum Router<T: Decodable> {
        case getRepoList(accountName: String)
        
        var path: String {
            get {
                switch self {
                case .getRepoList(accountName: let accountName):
                    return "/orgs/" + accountName + "/repos"
                }
            }
        }
        
        var method: String {
            get {
                switch self {
                case .getRepoList(accountName: _):
                    return "GET"
                }
            }
        }
    
        var GETParams: [URLQueryItem] {
            get {
                switch self {
                case .getRepoList(accountName: _):
                    return [URLQueryItem(name: "direction", value: "asc")]
                }
            }
        }
    }
    
    static func load<T: Decodable>(_ request: API.Router<T>, completion: @escaping (T?, RequestErrors?) -> (Void)) {
        
        guard Reachability.isConnectedToNetwork() else {
            completion(nil, RequestErrors.noInternetConnection)
            return 
        }
        
        let urlString = Environment.apiUrlProd + request.path
        guard var urlComponents = URLComponents(string: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "") else {
            completion(nil, RequestErrors.badURL(urlString: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""))
            return
        }
    
        urlComponents.queryItems = request.GETParams
        guard let url = urlComponents.url else {
            completion(nil, RequestErrors.badURL(urlString: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""))
            return
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = request.method
        let urlSession = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            // some error handling, TODO: better.
            guard error == nil else { completion(nil, RequestErrors.someError); return }
            
            guard let data = data else { completion(nil, RequestErrors.noData); return }
            
            do {
                let decodedData = try JSONDecoder().decode(T.self, from: data)
                
                if let error = error {
                    completion(decodedData, RequestErrors.decodingError(error))
                } else {
                    completion(decodedData, nil)
                }
                
            } catch let decodingError {
                completion(nil, RequestErrors.decodingError(decodingError))
                return
            }
        }
        urlSession.resume()
    }
}
