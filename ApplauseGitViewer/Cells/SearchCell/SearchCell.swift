//
//  SearchCell.swift
//  ApplauseGitViewer
//
//  Created by mac on 26/06/2019.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

protocol SearchCellDelegate: class {
    func searchValueChanged(_ searchField: UISearchBar?)
}

class SearchCell: UICollectionViewCell {
    
    static let identifier = "SearchCell"
    
    weak var searchTextField: UISearchBar?
    weak var delegate: SearchCellDelegate?
    
    override var reuseIdentifier: String? {
        return SearchCell.identifier
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupSearchTextField()
    }
    
    private func setupSearchTextField() {
        guard searchTextField == nil else { return }
        
        let search = UISearchBar()
        addSubview(search)
        searchTextField = search
        
        search.placeholder = "Type to search..."
        
        search.delegate = self
        search.autocapitalizationType = .none
        search.autocorrectionType = .no
        search.keyboardType = .default
        search.showsCancelButton = true
        
        search.clipsToBounds = true
        search.layer.cornerRadius = 10
        
        search.translatesAutoresizingMaskIntoConstraints = false
        search.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        search.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20).isActive = true
        search.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20).isActive = true
        search.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10).isActive = true
    }
}

extension SearchCell: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        delegate?.searchValueChanged(searchTextField)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

extension SearchCell {

    static func getCellSize(_ collectionView: UICollectionView) -> CGSize {
        let width = collectionView.bounds.width - collectionView.contentInset.left - collectionView.contentInset.right
        return CGSize(width: width, height: 64)
    }
}
