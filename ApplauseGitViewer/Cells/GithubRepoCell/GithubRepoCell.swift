//
//  GithubRepoCell.swift
//  ApplauseGitViewer
//
//  Created by mac on 26/06/2019.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class GithubRepoCell: UICollectionViewCell {
    
    static let identifier = "GithubRepoCell"
    
    @IBOutlet weak var titleLabel: UILabel!
    
    var model: (id: Int?, name: String?)? {
        didSet {
            titleLabel.text = model?.name ?? ""
        }
    }
}

extension GithubRepoCell {
    
    static func getCellSize(_ collectionView: UICollectionView) -> CGSize {
        let width = collectionView.bounds.width - collectionView.contentInset.left - collectionView.contentInset.right
        return CGSize(width: width, height: 50)
    }
}
